const { series, parallel, src, dest, watch } = require('gulp')
const rename = require('gulp-rename')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')
const gulpif = require('gulp-if')
const del = require('del')
const postcss = require('gulp-postcss')
const postcssPresetEnv = require('postcss-preset-env')
const postcssImport = require('postcss-import')
const postcssPixelsToRem = require('postcss-pixels-to-rem')
const postcssNested = require('postcss-nested')
const cssNano = require('cssnano')
const browserSync = require('browser-sync')
const imagemin = require('gulp-imagemin')
const imageminJpegRecompress = require('imagemin-jpeg-recompress')
const htmlmin = require('gulp-htmlmin')

const production = process.env.NODE_ENV === 'production'
const server = browserSync.create()

const paths = {
  dist: {
    html: 'dist/',
    js: 'dist/js/',
    css: 'dist/css/',
    images: 'dist/images/',
    fonts: 'dist/fonts/',
    vendor: 'dist/vendor/'
  },
  src: {
    html: 'src/*.html',
    js: 'src/js/main.js',
    css: 'src/css/index.pcss',
    images: 'src/images/**/*.*',
    fonts: 'src/fonts/**/*.*',
    vendor: 'src/vendor/**/*.*'
  },
  watch: {
    html: 'src/**/*.html',
    js: 'src/js/**/*.js',
    css: 'src/css/**/*.pcss',
    images: 'src/images/**/*.*',
    fonts: 'src/fonts/**/*.*',
    vendor: 'src/vendor/**/*.*'
  },
  clean: './dist'
}

const serve = (cb) => {
  server.init({
    server: {
      baseDir: paths.clean
    }
  })
  cb()
}

const reload = (cb) => {
  server.reload()
  cb()
}

const clean = () => {
  return del(paths.clean)
}

const styles = () => {
  const processors = [
    postcssImport(),
    postcssNested(),
    postcssPresetEnv({
      stage: 0,
      autoprefixer: {
        grid: 'autoplace'
      }
    })
  ]

  if (production) processors.push(postcssPixelsToRem(), cssNano())

  return src(paths.src.css, { sourcemaps: !production })
    .pipe(postcss(processors))
    .pipe(rename('styles.css'))
    .pipe(dest(paths.dist.css, { sourcemaps: '.' }))
}

const scripts = () => {
  return src(paths.src.js, { sourcemaps: !production })
    .pipe(babel())
    .pipe(gulpif(production, uglify()))
    .pipe(dest(paths.dist.js, { sourcemaps: '.' }))
}

const images = () => {
  return src(paths.src.images)
    .pipe(gulpif(production, imagemin([
      imagemin.gifsicle(),
      // imagemin.jpegtran({ progressive: true }),
      imageminJpegRecompress({
        loops: 4,
        min: 70,
        max: 80,
        quality: 'high'
      }),
      imagemin.optipng(),
      imagemin.svgo()
    ])))
    .pipe(dest(paths.dist.images))
}

const fonts = () => {
  return src(paths.src.fonts).pipe(dest(paths.dist.fonts))
}

const vendor = () => {
  return src(paths.src.vendor).pipe(dest(paths.dist.vendor))
}

const html = () => {
  return src(paths.src.html)
    .pipe(gulpif(production, htmlmin({ collapseWhitespace: true })))
    .pipe(dest(paths.dist.html))
}

const watcher = () => {
  watch(paths.watch.css, series(styles, reload))
  watch(paths.watch.js, series(scripts, reload))
  watch(paths.watch.images, series(images, reload))
  watch(paths.watch.fonts, series(fonts, reload))
  watch(paths.watch.vendor, series(vendor, reload))
  watch(paths.watch.html, series(html, reload))
}

exports.watch = series(serve, watcher)
exports.default = series(clean, parallel(styles, scripts, images, fonts, vendor, html))
